-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2018 at 08:26 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `srg`
--

-- --------------------------------------------------------

--
-- Table structure for table `dkfasilitas`
--

CREATE TABLE `dkfasilitas` (
  `id` int(10) NOT NULL,
  `gunung_id` int(10) NOT NULL,
  `kurang` float NOT NULL,
  `sedang` float NOT NULL,
  `lengkap` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dkfasilitas`
--

INSERT INTO `dkfasilitas` (`id`, `gunung_id`, `kurang`, `sedang`, `lengkap`) VALUES
(1, 5, 0, 0, 1),
(2, 6, 0, 0, 1),
(3, 7, 0, 1, 0),
(4, 8, 0, 0, 1),
(5, 9, 0, 0, 1),
(6, 10, 0, 0, 1),
(7, 11, 0, 0, 1),
(8, 12, 0, 0, 1),
(9, 13, 0, 0, 1),
(10, 14, 0, 0, 1),
(11, 15, 0, 0, 1),
(12, 16, 0, 0, 1),
(13, 17, 0, 0, 1),
(14, 18, 0, 0, 1),
(15, 19, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dkharga`
--

CREATE TABLE `dkharga` (
  `id` int(10) NOT NULL,
  `gunung_id` int(10) NOT NULL,
  `murah` float NOT NULL,
  `sedang` float NOT NULL,
  `mahal` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dkharga`
--

INSERT INTO `dkharga` (`id`, `gunung_id`, `murah`, `sedang`, `mahal`) VALUES
(1, 5, 0.8, 0.2, 0),
(2, 6, 0, 0.75, 0.25),
(3, 7, 1, 0, 0),
(4, 8, 0, 0.5, 0.5),
(5, 9, 0, 0, 1),
(6, 10, 0, 0, 1),
(7, 11, 0, 0.25, 0.75),
(8, 12, 0, 0.75, 0.25),
(9, 13, 0, 0.5, 0.5),
(10, 14, 0, 0, 1),
(11, 15, 0, 0, 1),
(12, 16, 1, 0, 0),
(13, 17, 1, 0, 0),
(14, 18, 1, 0, 0),
(15, 19, 0, 0.75, 0.25);

-- --------------------------------------------------------

--
-- Table structure for table `dkjamoperasional`
--

CREATE TABLE `dkjamoperasional` (
  `id` int(10) NOT NULL,
  `gunung_id` int(10) NOT NULL,
  `sebentar` float NOT NULL,
  `sedang` float NOT NULL,
  `lama` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dkjamoperasional`
--

INSERT INTO `dkjamoperasional` (`id`, `gunung_id`, `sebentar`, `sedang`, `lama`) VALUES
(1, 5, 0, 0.375, 0.625),
(2, 6, 0, 0.5, 0.5),
(3, 7, 0, 1, 0),
(4, 8, 0, 0, 1),
(5, 9, 1, 0, 0),
(6, 10, 0, 0, 1),
(7, 11, 0, 0.5, 0.5),
(8, 12, 0, 0.5, 0.5),
(9, 13, 0, 0.875, 0.125),
(10, 14, 1, 0, 0),
(11, 15, 0, 0, 1),
(12, 16, 0, 0, 1),
(13, 17, 0, 0, 1),
(14, 18, 0, 0.5, 0.5),
(15, 19, 0, 0.375, 0.625);

-- --------------------------------------------------------

--
-- Table structure for table `gunung`
--

CREATE TABLE `gunung` (
  `id` int(10) NOT NULL,
  `nama_gunung` varchar(40) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `harga` int(12) NOT NULL,
  `fasilitas` int(12) NOT NULL,
  `jamoperasional` int(12) NOT NULL,
  `alamat` text NOT NULL,
  `keterangan` text NOT NULL,
  `maps` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gunung`
--

INSERT INTO `gunung` (`id`, `nama_gunung`, `foto`, `harga`, `fasilitas`, `jamoperasional`, `alamat`, `keterangan`, `maps`) VALUES
(5, 'semeru', '1888594711.jpg', 36000, 75, 13, 'jawa', 'Gunung Semeru atau Gunung Meru adalah sebuah gunung berapi kerucut di Jawa Timur, Indonesia. Gunung Semeru merupakan gunung tertinggi di Pulau Jawa, dengan puncaknya Mahameru, 3.676 meter dari permukaan laut (mdpl). ... Kawah di puncak Gunung Semeru dikenal dengan nama Jonggring Saloko.', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15799.755088393256!2d112.92240748042913!3d-8.107717217748045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd63e89e1d817bb%3A0x9c14d4ed3c488f54!2sSemeru!5e0!3m2!1sen!2sid!4v1534458677709\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>'),
(6, 'manglayang', 'none.jpg', 50000, 10, 12, '', '', '0'),
(7, 'lembu', 'none.jpg', 30000, 5, 8, '', '', '0'),
(8, 'slamet', 'none.jpg', 60000, 60, 17, '', '', '0'),
(9, 'guntur', 'none.jpg', 100000, 20, 6, '', '', '0'),
(10, 'rinjani', 'none.jpg', 1000000, 90, 24, '', '', '0'),
(11, 'arjuno', 'none.jpg', 70000, 50, 12, '', '', '0'),
(12, 'prau', 'none.jpg', 50000, 37, 12, '', '', '0'),
(13, 'tangkuban perahu', 'none.jpg', 60000, 80, 9, '', '', '0'),
(14, 'krakatau', 'none.jpg', 100000, 39, 2, '', '', '0'),
(15, 'gede pangrango', 'none.jpg', 150000, 35, 20, '', '', '0'),
(16, 'lawu', 'none.jpg', 30000, 70, 20, '', '', '0'),
(17, 'merbabu', 'none.jpg', 10000, 68, 20, '', '', '0'),
(18, 'gunung mahal ', 'none.jpg', 1000, 100, 12, 'garut', 'test', 'test'),
(19, 'Gunung ciremai', '1744355847.jpg', 50000, 5, 13, 'Bantaragung, Sindangwangi, Kabupaten Majalengka, Jawa Barat', 'AGSGAHSDAHSDAHSDASDSADASD', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31687.817438142298!2d108.3891569683689!3d-6.8933328312068225!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6f22b433665d7f%3A0x4db097c51bab7431!2sGn.+Cereme!5e0!3m2!1sid!2sid!4v1535816831542\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>');

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE `kriteria` (
  `id` int(10) NOT NULL,
  `nama_kriteria` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`id`, `nama_kriteria`) VALUES
(1, 'harga'),
(2, 'fasilitas'),
(3, 'jam operasional');

-- --------------------------------------------------------

--
-- Table structure for table `setkriteria`
--

CREATE TABLE `setkriteria` (
  `id` int(10) NOT NULL,
  `kriteria_id` int(10) NOT NULL,
  `nama_setkriteria` varchar(30) NOT NULL,
  `nilai_minimal` int(12) DEFAULT NULL,
  `nilai_maksimal` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setkriteria`
--

INSERT INTO `setkriteria` (`id`, `kriteria_id`, `nama_setkriteria`, `nilai_minimal`, `nilai_maksimal`) VALUES
(1, 1, 'murah', NULL, 40000),
(2, 1, 'sedang', 35000, 80000),
(3, 1, 'mahal', 75000, NULL),
(4, 2, 'kurang', NULL, 5),
(5, 2, 'sedang', 4, 9),
(6, 2, 'lengkap', 8, NULL),
(7, 3, 'sebentar', NULL, 8),
(8, 3, 'sedang', 7, 16),
(9, 3, 'lama', 15, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`) VALUES
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dkfasilitas`
--
ALTER TABLE `dkfasilitas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dkfasilitas_ibfk_1` (`gunung_id`);

--
-- Indexes for table `dkharga`
--
ALTER TABLE `dkharga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dkharga_ibfk_1` (`gunung_id`);

--
-- Indexes for table `dkjamoperasional`
--
ALTER TABLE `dkjamoperasional`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gunung_id` (`gunung_id`);

--
-- Indexes for table `gunung`
--
ALTER TABLE `gunung`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setkriteria`
--
ALTER TABLE `setkriteria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dkfasilitas`
--
ALTER TABLE `dkfasilitas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `dkharga`
--
ALTER TABLE `dkharga`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `dkjamoperasional`
--
ALTER TABLE `dkjamoperasional`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `gunung`
--
ALTER TABLE `gunung`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `setkriteria`
--
ALTER TABLE `setkriteria`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dkfasilitas`
--
ALTER TABLE `dkfasilitas`
  ADD CONSTRAINT `dkfasilitas_ibfk_1` FOREIGN KEY (`gunung_id`) REFERENCES `gunung` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `dkharga`
--
ALTER TABLE `dkharga`
  ADD CONSTRAINT `dkharga_ibfk_1` FOREIGN KEY (`gunung_id`) REFERENCES `gunung` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `dkjamoperasional`
--
ALTER TABLE `dkjamoperasional`
  ADD CONSTRAINT `dkjamoperasional_ibfk_1` FOREIGN KEY (`gunung_id`) REFERENCES `gunung` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
