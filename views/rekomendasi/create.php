        <form method='post' action='rekomendasi.php?act=proses'>
            <div class="form-group">
                <label >Harga</label>
                <select class="form-control" name='harga'>
                    <option value='murah'>Murah 
                    (
                        Harga <
                        <?php $data=getsettingkriteria('harga','murah'); echo $data['nilai_maksimal'];?>
                    )
                    
                    </option>
                    <option value='sedang'>Sedang


                    (
                        <?php $data=getsettingkriteria('harga','sedang'); echo $data['nilai_minimal'];?> 
                        --
                        <?php $data=getsettingkriteria('harga','sedang'); echo $data['nilai_maksimal'];?>
                    )
                    
                    
                    </option>
                    <option value='mahal'>Mahal
                        
                    (
                        Harga >
                        <?php $data=getsettingkriteria('harga','mahal'); echo $data['nilai_minimal'];?>
                    )
                    
                    </option>
                </select>
            </div>
            <div class="form-group">
                <label >Fasilitas</label>
                <select class="form-control" name='fasilitas'>
                    <option value='kurang'>Kurang
                    
                    (
                        Fasilitas <
                        <?php $data=getsettingkriteria('fasilitas','kurang'); echo $data['nilai_maksimal'];?>/100
                    )
                    
                    </option>
                    <option value='sedang'>Sedang
                    
                    (
                        <?php $data=getsettingkriteria('fasilitas','sedang'); echo $data['nilai_minimal'];?>/100 
                        --
                        <?php $data=getsettingkriteria('fasilitas','sedang'); echo $data['nilai_maksimal'];?>/100
                    )
                    
                    
                    </option>
                    <option value='lengkap'>Lengkap
                    
                    (
                        Fasilitas >
                        <?php $data=getsettingkriteria('fasilitas','lengkap'); echo $data['nilai_minimal'];?>/100
                    )
                    
                    </option>
                </select>
            </div>

            <div class="form-group">
                <label >Jam Operasional</label>
                <select class="form-control" name='jamoperasional'>
                    <option value='sebentar'>Sebentar
                    
                    (
                        Jam Operasional >
                        <?php $data=getsettingkriteria('jam operasional','sebentar'); echo $data['nilai_maksimal'];?> jam
                    )
                    
                    
                    </option>
                    <option value='sedang'>Sedang
                    
                    (
                        <?php $data=getsettingkriteria('jam operasional','sedang'); echo $data['nilai_minimal'];?> jam 
                        --
                        <?php $data=getsettingkriteria('jam operasional','sedang'); echo $data['nilai_maksimal'];?> jam
                    )
                    
                    </option>
                    <option value='lama'>Lama
                    
                    (
                        Jam Operasional >
                        <?php $data=getsettingkriteria('jam operasional','lama'); echo $data['nilai_minimal'];?> jam
                    )
                    
                    </option>
                </select>
            </div>

            
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>